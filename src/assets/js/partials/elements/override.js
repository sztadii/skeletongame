String.prototype.setChar = function(num, char) {
    if(num > this.length - 1)
        return this.toString();

    else
        return this.substr(0, num) + char + this.substr(num + 1);
};