function afterStart() {
    container.classList.add("animation");

    setTimeout(function () {
        container.classList.remove("animation");
        setTimer();
    }, animationDuration);
}

function beforeFinish() {
    container.classList.add("animation");
}

function setVariables(option) {
    setHiddenWord();

    word = setWord();
    hiddenWord = hideWord(word);
    letters = setLetters();
    fails = 0;
    time = 60;
    alphabet = document.getElementById("alphabet");
    board = document.getElementById("board");
    gallows = document.getElementById("gallows");

    if (option == "reset") {
        amountPositive = 0;
        amountNegative = 0;
    }
}

function printGallows() {
    var img = '<img src="assets/img/s' + fails + '.jpg" alt="gallows"/>';

    gallows.innerHTML = img;
}

function printAlphabet() {
    var divContent = "",
        nameId = "";

    for (var i = 0; i < letters.length; i++) {
        nameId = "let" + i;
        divContent += '<div class="letter button" onclick="checkLetter(' + i + ')" id="' + nameId + '">' + letters[i] + '</div>';

        if ((i + 1) % 7 == 0) {
            divContent += '<div class="cleaner"></div>';
        }
    }

    alphabet.innerHTML = divContent;
}

function printWord() {
    word = word.toUpperCase();

    board.innerHTML = hiddenWord;
}

function printTime() {
    var timeMessage = '<div class="timer">' + time + '</div>';
    timer.innerHTML = timeMessage;
}

function printAmount() {
    var amountMessage = '<div class="amount positive">' + amountPositive + '</div>';
    amountMessage += '<div class="amount negative">' + amountNegative + '</div>';
    amounts.innerHTML = amountMessage;
}
