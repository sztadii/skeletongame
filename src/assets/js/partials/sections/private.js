function setWord() {
    var newWord = "",
        random = getRandomInt(1, 10);

    switch (random) {
        case 1:
            newWord = "Apetyt rośnie w miarę jedzenia";
            break;
        case 2:
            newWord = "Bez pracy nie ma kołaczy";
            break;
        case 3:
            newWord = "Cel uświęca środki";
            break;
        case 4:
            newWord = "Co za dużo to niezdrowo";
            break;
        case 5:
            newWord = "Dla chcącego nic trudnego";
            break;
        case 6:
            newWord = "Głupi ma zawsze szczęście";
            break;
        case 7:
            newWord = "Jak kamień w wodę";
            break;
        case 8:
            newWord = "Kłamstwo ma krótkie nogi";
            break;
        case 9:
            newWord = "Kto pierwszy ten lepszy";
            break;
        default:
            newWord = "Pierwsze koty za płoty";
    }

    return newWord;
}

function setTimer() {
    setTimeout(function () {
        if (time-- == 0) {
            fails = 9;
            time = 60;
        }
        if (!isFinish()) {
            printTime();
            setTimer();
        }
    }, 1000);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function hideWord(oldWord) {
    var newWord = "";

    for (var i = 0; i < oldWord.length; i++) {
        if (oldWord.charAt(i) == " ")
            newWord += " ";
        else
            newWord += "-";
    }

    return newWord;
}

function setLetters() {
    var letters = new Array(35);

    letters[0] = "A";
    letters[1] = "Ą";
    letters[2] = "B";
    letters[3] = "C";
    letters[4] = "Ć";
    letters[5] = "D";
    letters[6] = "E";
    letters[7] = "Ę";
    letters[8] = "F";
    letters[9] = "G";
    letters[10] = "H";
    letters[11] = "I";
    letters[12] = "J";
    letters[13] = "K";
    letters[14] = "L";
    letters[15] = "Ł";
    letters[16] = "M";
    letters[17] = "N";
    letters[18] = "Ń";
    letters[19] = "O";
    letters[20] = "Ó";
    letters[21] = "P";
    letters[22] = "Q";
    letters[23] = "R";
    letters[24] = "S";
    letters[25] = "Ś";
    letters[26] = "T";
    letters[27] = "U";
    letters[28] = "V";
    letters[29] = "W";
    letters[30] = "X";
    letters[31] = "Y";
    letters[32] = "Z";
    letters[33] = "Ż";
    letters[34] = "Ź";

    return letters;
}

function checkLetter(nr) {
    var hit = false;

    for (var i = 0; i < word.length; i++) {
        if (word.charAt(i) == letters[nr]) {
            hiddenWord = hiddenWord.setChar(i, letters[nr]);
            hit = true;
        }
    }

    if (hit == true) {
        setLetter(nr, "hitted");
        printWord();
    } else {
        fails++;
        setLetter(nr, "nothitted");
        printGallows();
    }

    isFinish();
}

function isFinish() {
    if (fails == 9) {
        finishGame("negative");
        return true;
    } else if (word == hiddenWord) {
        finishGame("positive");
        return true;
    } else {
        return false;
    }
}

function setLetter(nr, className) {
    var nameId = "let" + nr,
        letter = document.getElementById(nameId);

    letter.classList.add(className);
    letter.removeAttribute("onclick");
}

function finishGame(type) {
    var message = "";
    hiddenWord = word;

    if (type == "positive") {
        message = '<div class="message">Brawo, podałeś poprawne słowo!</div>';
        amountPositive++;
    }
    else if (type == "negative") {
        message = '<div class="message">Słabo, dzisiaj nie masz szczęścia.</div>';
        amountNegative++;
    }

    message += '<div class="reset button" onclick="resetGame()">Spróbuj jeszcze raz!</div>';
    alphabet.innerHTML = message;

    printWord();
    printAmount();
    setHiddenWord(type);
}

function setHiddenWord(type) {
    if (type == "positive")
        board.classList.add("positive");
    else if (type == "negative")
        board.classList.add("negative");
    else
        board.classList.remove("negative", "positive");
}

function resetGame() {
    beforeFinish();

    setTimeout(function () {
        appStart();
    }, animationDuration);
}
