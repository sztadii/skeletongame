//VARIABLES
var word;
var hiddenWord;
var letters;
var fails;
var time;
var animationDuration = 500;
var amountPositive = 0;
var amountNegative = 0;
var container = document.getElementById("container");
var alphabet = document.getElementById("alphabet");
var board = document.getElementById("board");
var gallows = document.getElementById("gallows");
var amounts = document.getElementById("amounts");
var timer = document.getElementById("timer");