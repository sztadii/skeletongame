function appStart() {
    afterStart();
    setVariables("start");
    printGallows();
    printWord();
    printAlphabet();
    printTime();
    printAmount();
}

window.onload = appStart();