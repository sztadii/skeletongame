'use strict';

var $ = require('gulp-load-plugins')();
var gulp = require('gulp');
var cleancss = require('gulp-clean-css');
var rimraf = require('gulp-rimraf');
var ignore = require('gulp-ignore');
var htmlmin = require('gulp-htmlmin');
var gcmq = require('gulp-group-css-media-queries');
var rename = require("gulp-rename");
var imports = require('gulp-imports');
var argv = require('yargs').argv;
var browser = require('browser-sync').create();
var panini = require('panini');
var sequence = require('run-sequence');

// Check for --production flag
var isProduction = !!(argv.production);

// Port to use for the development server.
var PORT = {
  local: 8000,
  ui: 3000
};

// Port to use for the development server.

var DIST = {
  dir: 'dist/',
  assetsDir: 'dist/assets/',
  cssDir: 'dist/assets/css/',
  imgDir: 'dist/assets/img/',
  jsDir: 'dist/assets/js/',
  cssFile: 'app.css',
  jsFile: 'app.js'
};

// Browsers to target when prefixing CSS.
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// File paths to various assets are defined here.
var PATHS = {
  assets: [
    'src/assets/**/*',
    '!src/assets/{!img,js,sass,css}{,/**/*}'
  ],
  css: [
    'src/assets/css/index-sass.css'
  ],
  javascript: [
    'src/assets/js/index.js'
  ]
};

// Delete the "dist" folder
// This happens every time a build starts
gulp.task('clean', function () {
  return gulp.src(DIST.dir, {read: false})
      .pipe(rimraf());
});

gulp.task('clean:img', function () {
  return gulp.src('dist/assets/img/*', {read: false})
      .pipe(rimraf());
});

gulp.task('clean:pages', function () {
  return gulp.src(DIST.dir + '**.html', {read: false})
      .pipe(ignore(DIST.assetsDir))
      .pipe(rimraf());
});

// Copy files out of the assets folder
// This task skips over the "img", "js", and "sass" folders, which are parsed separately
gulp.task('copy', function () {
  gulp.src(PATHS.assets)
      .pipe(gulp.dest(DIST.assetsDir));
});

// Copy currentSection templates into finished HTML files
gulp.task('pages', function () {
  var options = {
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    removeComments: true,
    removeAttributeQuotes: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true
  };

  var minify = $.if(isProduction, htmlmin(options));

  gulp.src('src/pages/**/*.{html,hbs,handlebars}')
      .pipe(panini({
        root: 'src/pages/',
        layouts: 'src/layouts/',
        partials: 'src/partials/',
        data: 'src/data/'
      }))
      .pipe(minify)
      .pipe(gulp.dest(DIST.dir));
});

gulp.task('pages:reset', function () {
  panini.refresh();
  gulp.run('pages');
});

// Compile Sass into CSS
gulp.task('sass', function () {

  return gulp.src('./src/assets/sass/index.sass')
      .pipe($.sass().on('error', $.sass.logError))
      .pipe($.autoprefixer({
        browsers: COMPATIBILITY
      }))
      .pipe(rename("index-sass.css"))
      .pipe(gulp.dest('./src/assets/css/'));
});

// Concat all css
// In production, the file is minified
gulp.task('css', function () {
  var cssOptions = {
    keepSpecialComments: 0 //Remove special comments
  };

  return gulp.src(PATHS.css)
      .pipe($.concat(DIST.cssFile))
      .pipe($.if(isProduction, gcmq()))
      .pipe($.if(isProduction, cleancss(cssOptions)))
      .pipe(gulp.dest(DIST.cssDir));
});

// Run tast sass, css
gulp.task('styles', function (done) {
  sequence('sass', ['css'], done);
});

// Combine JavaScript into one file
// In production, the file is minified
gulp.task('javascript', function () {
  var uglify = $.if(isProduction, $.uglify()
      .on('error', function (e) {
        console.log(e);
      }));

  return gulp.src(PATHS.javascript)
      .pipe(imports())
      .pipe($.if(!isProduction, $.sourcemaps.init()))
      .pipe($.concat(DIST.jsFile))
      .pipe(uglify)
      .pipe($.if(!isProduction, $.sourcemaps.write()))
      .pipe(gulp.dest(DIST.jsDir));
});

// Copy images to the "dist" folder
// In production, the images are compressed
gulp.task('images', function () {
  var imagemin = $.if(isProduction, $.imagemin({
    progressive: true
  }));

  return gulp.src('src/assets/img/**/*')
      .pipe(imagemin)
      .pipe(gulp.dest(DIST.imgDir));
});

// Build the "dist" folder by running all of the above tasks
gulp.task('build', function (done) {
  sequence('clean', ['pages', 'styles', 'javascript', 'images', 'copy'], done);
});

// Start a server with LiveReload to preview the site in
gulp.task('server', ['build'], function () {
  browser.init({
    server: DIST.dir,
    reloadDelay: 1000,
    port: PORT.local,
    open: 'local',
    ui: {
      port: PORT.ui
    },
    plugins: ['bs-latency']
  });
});

// Build the site, run the server, and watch for file changes
gulp.task('default', ['build', 'server'], function () {
  gulp.watch(PATHS.assets, ['copy', browser.reload]);
  gulp.watch(['src/pages/**/*.html'], ['clean:pages', 'pages', browser.reload]);
  gulp.watch(['src/{layouts,partials}/**/*.html'], ['pages:reset', browser.reload]);
  gulp.watch(['src/bundles/**/*'], ['styles', 'javascript', browser.reload]);
  gulp.watch(['src/data/**/*'], ['clean:pages', 'pages:reset', browser.reload]);
  gulp.watch(['src/assets/sass/**/*.sass'], ['sass']);
  gulp.watch(PATHS.css, ['styles', browser.reload]);
  gulp.watch(['src/assets/js/**/*.js'], ['javascript', browser.reload]);
  gulp.watch(['src/assets/img/**/*'], ['clean:img', 'images', browser.reload]);
});